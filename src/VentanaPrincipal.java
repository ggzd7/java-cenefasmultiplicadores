package pryCenefasPromoTexto;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class VentanaPrincipal extends javax.swing.JFrame {

    int formato = 0, cantidad = 0;
    double valor = 0;
    String a = "", textoselec = "", txtcombo = "", txtcombotipo = "";
    String pathformatos = "c:\\nicesmx\\Textocenefas.txt";
    String pathtipomulti = "c:\\nicesmx\\TipoMultiplicador.txt";
    String pathcenefas = "c:\\nicesmx\\sgcenefa.all";
    String pathsalida = "c:\\nicesmx\\sxcenefa.txt";
    String pathejecutable = "";
    //String pathejecutable = "c:\\nicesmx\\ceprotxt.bat";
    FileReader fr;
    BufferedReader br;
    FileWriter fw;
    BufferedWriter bw;
    Runtime Imprimebat = Runtime.getRuntime();
    String letras = "";
    String numeros = "";
    String fecha = "";
    Robot robot;
    int numeroformateado = 0;
    double numcalculos = 0;
    private boolean hayNumeros = false;
    private boolean hayLetras = false;
    
    private static boolean isNumeric(char caracter){
        try{
            Integer.parseInt(String.valueOf(caracter));
            return true;
        }
        catch(NumberFormatException ex){
            return false;
        }
    }    

    public boolean valida_fecha(String fecha){
        if((fecha.substring(2,3)).compareTo("-")==0 && (fecha.substring(5,6)).compareTo("-")==0){
            int año = Integer.parseInt(fecha.substring(6));
            int mes = Integer.parseInt(fecha.substring(3,5));
            int dia = Integer.parseInt(fecha.substring(0,2));
            if (año > 1900) {
                if (mes > 0 && mes < 13) {
                    int tope;
                    if (mes == 1 || mes == 3 || mes == 5 || mes == 7 ||mes == 8 || mes == 10 || mes == 12) {
                        tope = 31;
                    } else if (mes == 2) {
                        if (año % 4 == 0) {
                            tope = 29; //es bisiesto
                        } else tope = 28;
                    } else tope = 30;
                        if (dia > 0 && dia < tope + 1) {
                            return true;
                        } else {
                            //JOptionPane.showMessageDialog(null,"La fecha no es correcta");
                            return false;
                        }
                } else {
                //JOptionPane.showMessageDialog(null,"La fecha no es correcta");
                return false;
                }
            } else {
                //JOptionPane.showMessageDialog(null,"La fecha no es correcta");
                return false;
            }
        } else{
            //JOptionPane.showMessageDialog(null,"La fecha no es correcta");
            return false;
        }
    }
    
    public VentanaPrincipal() {
        
        this.setTitle("CENEFAS PROMOCION TEXTO");
        initComponents();
        jComboBox1.setEditable(false);
        jLabel2.setVisible(false);
        jLabel3.setText("");
        jLabel3.setVisible(false);
        jTextField1.setVisible(false);        
        try{
            fr = new FileReader(pathformatos);
            br = new BufferedReader(fr);
            //jComboBox1.addItem("Elegir Texto...");
            a = "";
            String[] b;
            while((a = br.readLine()) != null){
                if(a.indexOf(",") > 0){
                    b = a.split(",");
                    jComboBox1.addItem("" + b[1]);
                    System.out.println("Elemento...ok" + b[1]);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Error: Por favor revise archivo de parametros.");
                    //System.out.println("No hay comas o lineas en parametros...");
                }
            }
            br.close();
            fr.close();
            CargaTipo();
            jComboBox1.requestFocus();
        }catch(IOException ioe){
            System.out.println("Error:" + ioe);
        }
    }
    
    public void CargaTipo(){
        try{
            fr = new FileReader(pathtipomulti);
            br = new BufferedReader(fr);
            a = "";
            String[] b = null;
            while((a = br.readLine()) != null){
                if(a.indexOf(",") > 0){
                    b = a.split(",");
                    jComboBoxTipo.addItem("" + b[0]);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Error: Por favor revise archivo de parametros \nTipo Multiplicador.");
                }
            }
            br.close();
            fr.close();
        }catch(IOException ioe){
            System.out.println("Error:" + ioe);
        }
    }
    
    public String SaberTipo(){
        int seleccion = jComboBoxTipo.getSelectedIndex();
        System.out.println("Selecionado: " + jComboBoxTipo.getSelectedItem().toString() + " Indice: " + jComboBoxTipo.getSelectedIndex());
        String ejecuta = "";
        int cont = 1;
        try{
            fr = new FileReader(pathtipomulti);
            br = new BufferedReader(fr);
            a = "";
            String[] b = null;
            do{
                a = br.readLine();
                if(a.indexOf(",") > 0){
                    b = a.split(",");
                    ejecuta = b[1];
                }
                else{
                    JOptionPane.showMessageDialog(null,"Error: Por favor revise archivo de parametros \nTipo Multiplicador.");
                }
                cont++;
                //System.out.println("cont: " + cont);
                //System.out.println("seleccion: " + seleccion);
            }while(cont == seleccion);
            br.close();
            fr.close();
            cont = 0;
        }catch(IOException ioe){
            System.out.println("Error:" + ioe);
        }
        //System.out.println("Path Ejecutable Cenefa: " + ejecuta);
        return ejecuta;
    }
    
    private void CreaRobot(){
        try{
                
            robot = new Robot();
                
        }catch (AWTException e) {
                
            e.printStackTrace();
        }
    }
    
    public String espacios(String txt1, int tam){
        if(tam == 20){
            if(txt1.length() <= tam){
                do{
                    txt1 = txt1 + " ";
                }while(txt1.length() < tam);
                
            }else if(txt1.length() > tam){
                txt1 = txt1.substring(0, tam);
            }
        }else if(tam == 8){
            if(txt1.length() <= tam){
                do{
                    txt1 = " " + txt1;
                }while(txt1.length() < tam);
                
            }else if(txt1.length() > tam){
                txt1 = txt1.substring(0, tam);
            }
        }
        System.out.println("================================");
        System.out.println("Linea: " + txt1);
        System.out.println("Tamaño linea: " + txt1.length());
        return txt1;
    }     
    
    public String calculos(String value, String index){
        double aux = 0;
        numeros = "";
        letras = "";
        hayNumeros = false;
        hayLetras = false;
        for(short indice = 0; indice < index.length(); indice++){
            char caracter = index.charAt(indice);
            if(isNumeric(caracter)){
                numeros += caracter;
            }
            else{
                letras += caracter;
            }
        }
        letras = letras.trim();
        if(value.isEmpty() != true){
            numcalculos = Double.parseDouble(value);
        }
        
        if(numeros.isEmpty() != true){
            numeroformateado = Integer.parseInt(numeros);
            hayNumeros = true;
        }
        
        if(letras.isEmpty() != true){
            hayLetras = true;
        }
        
        if(hayNumeros == true && hayLetras == false){
            aux = numcalculos*numeroformateado;
            //System.out.println("aux= " + aux);
            value = String.valueOf(aux);
            System.out.println(numcalculos + "*" + numeroformateado + "=" + value);
            value = String.format("%3.2f", aux).replace(".00", "");
        }
        else if(hayNumeros == false && hayLetras == true){
            //JOptionPane.showMessageDialog(null, "No existe numeros para calculos: " + hayLetras);
            System.out.println("No existe numeros para calculos");
        }
        else {
            //precio = (precio/2)+(precio*(cant-1))
            if(numeroformateado == 0){
                value = " ";
            }else{
                aux = (numcalculos/2) + (numcalculos*(numeroformateado - 1));
                //value = String.valueOf(aux);
                //System.out.println("NUMCALCULOS: " + numcalculos + "\nNUMEROFORMATEADO: " + numeroformateado+ "\n="+aux);
                value = String.format("%3.2f", aux);
                //System.out.println("Calculo letras: " + value);
            }
        }
        value = value.replace(",", "");
        value = value.replace(".", "");
        value = espacios(value, 8);
        //System.out.println("Valor: " + value);
        //System.out.println("Tamaño valor: " + value.length());
        System.out.println("================================");
        return value;
    }           
    
    public void completatexto (String txt, String fecha, String index) {
        int cont = 0;
        String entero = "";
        String decimal = "";
        String precio = "";
        txt = espacios(txt, 20);
        try{
            fr = new FileReader(pathcenefas);
            br = new BufferedReader(fr);
            fw = new FileWriter(pathsalida);
            bw = new BufferedWriter(fw);
            a = "";
            while((a = br.readLine()) != null){
                entero = a.substring(94, 100);
                decimal = a.substring(100, 102);
                precio = entero + "." + decimal;
                /*System.out.println("entero: " + entero + " tam: " + entero.length());
                System.out.println("decimal: " + decimal + " tam: " + decimal.length());
                System.out.println("Precio: " + precio + " tam: " + precio.length());*/
                
                precio = calculos(precio,index);
                a = a + txt + precio + fecha;
                cont = cont + 1;
                bw.write(a);
                bw.newLine();
            }
            bw.close();
            fw.close();
            br.close();
            fr.close();
            pathejecutable = SaberTipo();
            System.out.println("path: " + pathejecutable);
            Imprimebat.exec("cmd /c start " + pathejecutable);
            jLabel3.setText("Proceso OK, \nProcesados: " + cont + " registros");
            jLabel3.setVisible(true);
            cont = 0;
            jComboBoxTipo.setSelectedIndex(0);
        }catch(IOException ioe){
            jLabel3.setText("Error en Proceso");
            jLabel3.setVisible(true);
        }

    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox();
        jTextField1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jFormattedTextField1 = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabelTipo = new javax.swing.JLabel();
        jComboBoxTipo = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("TEXTO:");
        jLabel1.setToolTipText("");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Ingrese Texto max. 18 caracteres");
        jLabel2.setToolTipText("");

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Procesar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Elegir Texto..." }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1KeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("FECHA:");

        try {
            jFormattedTextField1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##-##-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextField1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jFormattedTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jFormattedTextField1KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jFormattedTextField1KeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        jLabel5.setText("dd-MM-AAAA");

        jLabelTipo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelTipo.setText("TIPO:");
        jLabelTipo.setToolTipText("");

        jComboBoxTipo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBoxTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Elegir Tipo..." }));
        jComboBoxTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxTipoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabelTipo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBoxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jTextField1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(32, 32, 32)
                                .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(11, 11, 11)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTipo)
                    .addComponent(jComboBoxTipo, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        txtcombo = (String) jComboBox1.getSelectedItem();
        txtcombotipo =(String) jComboBoxTipo.getSelectedItem();
        formato = jComboBox1.getSelectedIndex();
        fecha = jFormattedTextField1.getText();
        int conta = 0;
        a = "";
        if(txtcombo.contains("Elegir")){
            JOptionPane.showMessageDialog(null,"Seleccione texto");
        }
        else if(txtcombotipo.contains("Elegir")){
            JOptionPane.showMessageDialog(null,"Seleccione tipo");
        }
        else if(fecha.trim().length() < 10){
            JOptionPane.showMessageDialog(null,"Ingrese Fecha");
        }else if(valida_fecha(fecha)==true){
            if (formato == 0){
                JOptionPane.showMessageDialog(null,"Eligir Texto");
            }else{
                if( (txtcombo.contains("Elegir")) && (jTextField1.getText().length() > 0 )){
                    txtcombo = jTextField1.getText();
                    completatexto(txtcombo , fecha , "        ");
                    jComboBox1.setSelectedIndex(0);
                    jFormattedTextField1.setText("");
                    jComboBox1.requestFocus();
                }else if( (txtcombo.contains("Elegir")) && (jTextField1.getText().length() <= 0 )){
                    JOptionPane.showMessageDialog(null,"Ingrese Texto");
                }else{
                    try{
                        fr = new FileReader(pathformatos);
                        br = new BufferedReader(fr);
                        conta = formato;
                        a = "";
                        String[] b = null;
                        while(true){
                            a = br.readLine();
                            if(conta > 0){
                                if(a.indexOf(",") > 0){
                                    b = a.split(",");
                                    if(b[0].length() > 1){
                                        b[0] = b[0].substring(0, 2);
                                        txtcombo = b[0];
                                        conta = conta - 1;
                                    } else{
                                        JOptionPane.showMessageDialog(null,"Error: Por favor revise archivo de parametros.");
                                    }
                                } else{
                                    JOptionPane.showMessageDialog(null, "Novedad en el proceso, revise archivo de parametros");
                                }
                            } else{ 
                                break;
                            }
                        }
                    br.close();
                    fr.close();
                    txtcombo = txtcombo + (String) jComboBox1.getSelectedItem();
                    completatexto(txtcombo , fecha , b[0]);
                    jComboBoxTipo.setSelectedItem(0);
                    jComboBox1.setSelectedIndex(0);
                    jFormattedTextField1.setText("");
                    jComboBox1.requestFocus();
                }catch(IOException ioe){
                    System.out.println("Error: " + ioe);
                }
            }
        }
        }else{
            JOptionPane.showMessageDialog(null, "Formato de Fecha Incorrecto\nFormato: DD-MM-AAAA");
        }
        txtcombo = "";
        fecha = "";
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        //jLabel3.setText("");
        String textcombo = (String) jComboBox1.getSelectedItem();
        if (textcombo.contains("Ingresar")){
            jLabel2.setVisible(true);
            jLabel3.setVisible(false);
            jTextField1.setVisible(true);
            jTextField1.setEditable(true);
            jFormattedTextField1.requestFocus();
            //jTextField1.requestFocus();
        }
        else if(textcombo.isEmpty()){
            JOptionPane.showMessageDialog(null,"No existe texto");
            jLabel3.setVisible(false);
            jComboBox1.setSelectedIndex(0);
        }
        else if(textcombo.startsWith(" ")){
            JOptionPane.showMessageDialog(null,"No existe texto");
            jLabel3.setVisible(false);
            jComboBox1.setSelectedIndex(0);
        }
        else if (textcombo.contains("Elegir")){
            jFormattedTextField1.setText("");
        }
        else{
            jLabel2.setVisible(false);
            jLabel3.setVisible(false);
            jTextField1.setVisible(false);
            jTextField1.setText("");
            jFormattedTextField1.setText("");
            jFormattedTextField1.requestFocus();
        }

        
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyTyped
        // TODO add your handling code here:
        if (jTextField1.getText().length() <= 17){
        
        } else {
            evt.setKeyChar('\0');
        }
    }//GEN-LAST:event_jTextField1KeyTyped

    private void jFormattedTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedTextField1KeyPressed
        // TODO add your handling code here:
        CreaRobot();
        int aux1 = jFormattedTextField1.getText().trim().length();
        int tecla = evt.getKeyCode();
        if (tecla == KeyEvent.VK_ENTER){    
            if(aux1 == 10){
                robot.keyPress(KeyEvent.VK_TAB);
                evt.consume();
            }
            else{
                System.out.println("FECHA INCORRECTA");
                JOptionPane.showMessageDialog(null,"Fecha Incorrecta");
            }
        }
        
    }//GEN-LAST:event_jFormattedTextField1KeyPressed

    private void jFormattedTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedTextField1KeyTyped
        // TODO add your handling code here:
        /*if (jFormattedTextField1.getText().length() <= 10){
        
        } else {
            evt.setKeyChar('\0');
        } */   
    }//GEN-LAST:event_jFormattedTextField1KeyTyped

    private void jComboBoxTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxTipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxTipoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VentanaPrincipal ventana = new VentanaPrincipal();
                ventana.pack();//para que se vean bien los elemento de la ventana
                Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();//para obtener las dimensiones de la pantalla
                Dimension dimen = ventana.getSize();//para obtener las dimensiones de la ventana
                ventana.setLocation((pantalla.width - dimen.width) / 2,(pantalla.height - dimen.height) / 2);
                ventana.setResizable(false);
                ventana.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBoxTipo;
    private javax.swing.JFormattedTextField jFormattedTextField1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelTipo;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
